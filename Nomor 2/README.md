# nomor 2
Program download gambar kobeni

## Kobeni_liburan.sh
File kobeni_liburan merupakan program untuk menjawab soal nomor 2.

1. ```Kobeni_liburan.sh```
Berisi code berikut
```bash
#!/bin/bash

#check input
if (($# == 0))
then
	#Jika tidak ada masukkan input 1 1 1
	./kobeni_liburan.sh 1 1 1
else
	#Jika ada masukkan ke program
	x=$1
	y=$2
	z=$3

	#Buat variabel zipName
	zipName="devil_${x}"
	
	#Buat folder yang nantio akan dizip dan masuk ke dalamnya
	mkdir $zipName
	cd $zipName
	
	#Buat variabel berisi nama hari sekarang
	hari=$(date +%A)

	#Masuk ke loop selama hari masih sama
	while (("$(date +%A)" == "$hari"))
	do
		#Buat variabel nama folder untuk menyimpan gambar
		folderName="kumpulan_${y}"
		
		#Buat folder dan masuk ke dalamnya
		mkdir $folderName
		cd $folderName
		
		#Buat variabel berisi jam sekarang
		jam=$(date +%H)
		jam="${jam##*0}"
		
		#Masuk loop untuk mendownload sesuai dengan jam
		for((a=1; a<=$jam; a=$((a+1))))
		do
			#Mendownload file
			wget -O perjalanan_${z} "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyoF0p6XXl5v-YNDsTx1crlkxynURXFyYpUw&usqp=CAU"
			z=$((z+1))
		done
		
		#Kembali ke folder sebelumnya
		cd ..
		y=$((y+1))
		
		#Lanjut setelah 10 jam
		sleep 10h
	done
	
	#kembali ke folder sebelumnya lalu zip folder
	cd ..
	zip -r "$zipName" "$zipName"
	rm -r "$zipName"
	x=$((x+1))
fi
#Setelah ganti hari, jalankan program lagi dengan variable x y z
./kobeni_liburan.sh $x $y $z

```
Pertama 
```bash
if (($# == 0))
then
	#Jika tidak ada masukkan input 1 1 1
	./kobeni_liburan.sh 1 1 1
else
	#Jika ada masukkan ke program
    ...
fi
#Setelah ganti hari, jalankan program lagi dengan variable x y z
./kobeni_liburan.sh $x $y $z
```
Berguna untuk mengecek apakah file memiliki input atau tidak, ini berguna untuk looping harian program nanti.
File pertama kali dijalankan tanpa adanya input setelah sehari file akan memanggil file lagi dengan input x y z yang berupa nomor file

Setelahnya
```bash
    #Masuk ke loop selama hari masih sama
	while (("$(date +%A)" == "$hari"))
	do
		#Buat variabel nama folder untuk menyimpan gambar
		folderName="kumpulan_${y}"
		
		#Buat folder dan masuk ke dalamnya
		mkdir $folderName
		cd $folderName
		
		#Buat variabel berisi jam sekarang
		jam=$(date +%H)
		jam="${jam##*0}"
		
		#Masuk loop untuk mendownload sesuai dengan jam
		for((a=1; a<=$jam; a=$((a+1))))
		do
			#Mendownload file
			wget -O perjalanan_${z} "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyoF0p6XXl5v-YNDsTx1crlkxynURXFyYpUw&usqp=CAU"
			z=$((z+1))
		done
		
		#Kembali ke folder sebelumnya
		cd ..
		y=$((y+1))
		
		#Lanjut setelah 10 jam
		sleep 10h
	done
```
Berfungsi untuk looping kalau selama hari masih sama maka akan mendownload file dari link sebanyak waktu jam saat ini.
Setelah itu sleep selama 10 jam

```bash
#kembali ke folder sebelumnya lalu zip folder
	cd ..
	zip -r "$zipName" "$zipName"
	rm -r "$zipName"
	x=$((x+1))
```
Berguna untuk mengzip folder yang sudah dibuat pada satu hari

Berikut merupakan output dari program di atas:
![SS_Hasil_Terminal](/uploads/61b3d4a06f54f885ca5893e6cf681ad7/SS_Hasil_Terminal.png)

![SS_Hasil_Folder](/uploads/169d836f26d55f086119f790863bdd41/SS_Hasil_Folder.png)
