# BY ZEMETIA, REALLY I WOULD LIKE TO MAKE THIS MODULAR

## IMPORT SECTION & VARIABLE DECLARATION HAHA JUST FOR INFOMATION ##
usernames=($(cut -d "|" -f 1 users/users.txt)) #user username list 
in_username="" #username as input
in_password="" #password as input
match_index="" #the variable that will use to get user index
passchecked="" #variable to check the password 
log="" #temporary variable to store log

## LOG WRITING SECTION ##
function writeLog {
    stat=$1
    user=$2
    sche=$(date +"%y/%m/%d %T")

    log=""

    if [[ "$stat" == "INFO" ]]; then
        log="$sche REGISTER: INFO User $user registered successfully"
    else
        log="$sche REGISTER: ERROR User $user already exists"
    fi

    echo $log >> ./log.txt
}

## GET USER INDEX SECTION ##
function isExistUser {
    get_username=$1
    index=0

    for username in "${usernames[@]}"; do
        if [[ "$username" == "$get_username" ]]; then
            break
        else
            index=$((match_index+1))
        fi
    done

    match_index=$index
}

## PASSWORD CHECK SECTION ##
function checkPassword {
    get_username=$1
    get_password=$2

    # Check the length of the get_password
    if [[ ${#get_password} -lt 8 ]]; then
        passchecked="0"
        return
    fi

    #Check for at least one uppercase and one lowercase letter
    if [[ !($get_password =~ [A-Z] && $get_password =~ [a-z]) ]]; then
        passchecked="0"
        return
    fi

    # Check for alphanumeric characters only
    if [[ !($get_password =~ ^[[:alnum:]]*$) ]]; then
        passchecked="0"
        return
    fi

    # Check that the get_password is not the same as the get_username
    if [[ "$get_password" == "$get_username" ]]; then
        passchecked="0"
        return
    fi

    # Check that the get_password does not contain the words "chicken" or "ernie"
    if [[ "$get_password" == *"chicken"* || "$get_password" == *"ernie"* ]]; then
        passchecked="0"
        return
    fi
    

    # If all checks pass, the password is valid
    passchecked="1"
}

## REGISTER SECTION ##
function register {
    get_username=$1
    get_password=$2

    #Check if the user exist
    match_index=0
    isExistUser $get_username

    #Check the password pattern as the criteria
    passchecked=""
    checkPassword $get_username $get_password

    while [[ "$passchecked" == "0" ]]; do
        echo "Password tidak dapat digunakan, masukkan password yang berbeda"
        read -p "Password baru: " get_password
        echo "$get_password $passchecked"
        checkPassword $get_username $get_password
    done
    
    echo $match_index" ${#usernames[@]}" 
    if [[ "$((match_index - ${#usernames[@]}))" == "0" ]]; then
        echo "Berhasil daftar!"
        writeLog "INFO" $get_username
        echo "$get_username|$get_password" >> users/users.txt
    else 
        echo "Registrasi Gagal! Akun Sudah Ada"
        writeLog "ERROR" $get_username
    fi
}

## CODE CODE ##
register $1 $2