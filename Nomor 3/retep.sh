# BY ZEMETIA, REALLY I WOULD LIKE TO MAKE THIS MODULAR

## IMPORT SECTION & VARIABLE DECLARATION HAHA JUST FOR INFOMATION ##
usernames=($(cut -d "|" -f 1 users/users.txt)) #user username list 
passwords=($(cut -d "|" -f 2 users/users.txt)) #user password list
in_username="" #username as input
in_password="" #password as input
match_index="" #the variable that will use to get user index
passchecked="" #variable to check the password 
log="" #temporary variable to store log

## LOG WRITING SECTION ##
function writeLog() {
    stat=$1
    user=$2
    sche=$(date +"%y/%m/%d %T")

    log=""

    if [[ "$stat" == "INFO" ]]; then
        log="$sche LOGIN: INFO User $user logged in"
    else
        log="$sche LOGIN: ERROR Failed login attempt on user $user"
    fi

    echo $log >> ./log.txt
}

## GET USER INDEX SECTION ##
function isExistUser {
    get_username=$1
    index=0

    for username in "${usernames[@]}"; do
        if [[ "$username" == "$get_username" ]]; then
            break
        else
            index=$((match_index+1))
        fi
    done

    match_index=$index
}

## LOGIN SECTION ##
function login {
    get_username=$1
    get_password=$2

    match_index=0
    isExistUser $get_username
    
    if [[ "$((match_index - ${#usernames[@]}))" != "0" ]]; then
        if [[ "${passwords[$match_index]}" = "$get_password" ]]; then
            echo "Berhasil Masuk!"
            writeLog "INFO" $get_username
        else
            echo "Password Salah!"
            writeLog "ERROR" $get_username
        fi
    else
        echo "Akun tidak ada!"
    fi
}



## CODE CODE ##
login $1 $2



