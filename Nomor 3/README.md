# nomor 3
Sistem Login/Register Peter Griffin

## retep.sh
Pada file retep.sh berisi fungsi untuk melakukan login. disini saya menggunakan function untuk mempermudah strukturisasi programnya. ada beberapa function yang ada di file retep.sh antara lain ```writeLog```, ```isExistUser```, dan ```login```. Lalu dibawah akan di bahas untuk penggunaan masing masing fungsi

1. ```writeLog```
Fungsi writeLog berisi code berikut
```bash
function writeLog() {
    stat=$1
    user=$2
    sche=$(date +"%y/%m/%d %T")

    log=""

    if [[ "$stat" == "INFO" ]]; then
        log="$sche LOGIN: INFO User $user logged in"
    else
        log="$sche LOGIN: ERROR Failed login attempt on user $user"
    fi

    echo $log >> ./log.txt
}
```
menerima 2 argument, yaitu ```stat``` alias status, dan ```user``` yaitu username. memiliki 2 status yaitu INFO dan ERROR. lalu informasi log akan di simpan ke variable dan di push ke dalam file bernama log.txt

2. ```isExistUser```
Fungsi isExistUser berisi code berikut
```bash
function isExistUser {
    get_username=$1
    index=0

    for username in "${usernames[@]}"; do
        if [[ "$username" == "$get_username" ]]; then
            break
        else
            index=$((match_index+1))
        fi
    done

    match_index=$index
}
```
Fungsi ini awalnya ingin saya gunakan hanya untuk mengetahui apakah user sudah ada didalam file atau tidak. Disini saya sengaja tidak menggunakan AWK karena saya duga pasti kebanyakan sudah menggunakan AWK karena mudah dan tidak riber (saya suka yang ribet wkwkwk). Disini match_index akan bernilai index dimana user ditemukan

3. ```login```
Fungsi login berisi code berikut
```bash
function login {
    get_username=$1
    get_password=$2

    match_index=0
    isExistUser $get_username
    
    if [[ "$((match_index - ${#usernames[@]}))" != "0" ]]; then
        if [[ "${passwords[$match_index]}" = "$get_password" ]]; then
            echo "Berhasil Masuk!"
            writeLog "INFO" $get_username
        else
            echo "Password Salah!"
            writeLog "ERROR" $get_username
        fi
    else
        echo "Akun tidak ada!"
    fi
}
```
fungsi ini menerima 2 argumen yaitu username dan password, lalu username akan di check dan di dapatkan indexnya dengan fungsi ```isExistUser```, setelah itu akan di check, jika index - len(array username) == 0 berarti tidak ada user yang di cari atau iterasi dari isUserExist sudah lebih dari banyak user yang ada (alias tidak ditemukan), lalu kita mendapatkan password dengan mudah yaitu dengan index yang kita dapat dari username tadi dan mencocokkan password yang ada dengan password yang di input User lalu mengirimkan log.

## louis.sh
File louis.sh berisi fungsi untuk register user baru. yang berisi fungsi fungsi yang lebih banyak dari retep.sh yaitu ```writeLog```, ```isExistUser```, ```checkPassword```, dan ```register```.\
Fungsi ```writeLog``` adalah fungsi yang sama dengan ```writeLog``` yang ada di retep.sh dengan perbedaan message yang ada didalamnya. ```isExistUser``` adalah fungsi yang sama persis dengan yang ada di dalam file retep.sh
1. ```checkPassword```
Fungsi checkPassword berisi code berikut
```bash
function checkPassword {
    get_username=$1
    get_password=$2

    # Check the length of the get_password
    if [[ ${#get_password} -lt 8 ]]; then
        passchecked="0"
        return
    fi

    #Check for at least one uppercase and one lowercase letter
    if [[ !($get_password =~ [A-Z] && $get_password =~ [a-z]) ]]; then
        passchecked="0"
        return
    fi

    # Check for alphanumeric characters only
    if [[ !($get_password =~ ^[[:alnum:]]*$) ]]; then
        passchecked="0"
        return
    fi

    # Check that the get_password is not the same as the get_username
    if [[ "$get_password" == "$get_username" ]]; then
        passchecked="0"
        return
    fi

    # Check that the get_password does not contain the words "chicken" or "ernie"
    if [[ "$get_password" == *"chicken"* || "$get_password" == *"ernie"* ]]; then
        passchecked="0"
        return
    fi
    

    # If all checks pass, the password is valid
    passchecked="1"
}
```
disini saya menggunakan fungsi bukan hanya karna terstruktur tetapi juga memanfaatkan command ```return```. untuk setiap conditional memiliki comment diatasnya yang mendeskripsikan kegunaan tiap-tiap if yang ada. saya menggunakan variable ```passchecked``` untuk mengetahui apakah password sudah sesuai atau belum

2. ```register```
fungsi register berisi code berikut 
```bash
function register {
    get_username=$1
    get_password=$2

    #Check if the user exist
    match_index=0
    isExistUser $get_username

    #Check the password pattern as the criteria
    passchecked=""
    checkPassword $get_username $get_password

    while [[ "$passchecked" == "0" ]]; do
        echo "Password tidak dapat digunakan, masukkan password yang berbeda"
        read -p "Password baru: " get_password
        echo "$get_password $passchecked"
        checkPassword $get_username $get_password
    done
    
    echo $is_exist_user
    if [[ "$((match_index - ${#usernames[@]}))" == "0" ]]; then
        echo "Berhasil daftar!"
        writeLog "INFO" $get_username
        echo "$get_username|$get_password" >> users/users.txt
    else 
        echo "Registrasi Gagal! Akun Sudah Ada"
        writeLog "ERROR" $get_username
    fi
}
```
fungsi register ini menerima 2 argument yaitu username dan password, lalu sebelumnya kita mengecek apakah user ada atau tidak, lalu disimpan dulu. setelah itu untuk berjaga jaga kita deklarasi kembali ```passchecked=""``` agar memastikan passchecked kosong, lalu call fungsi checkPassword. Jika password yang di berikan belum sesuai dengan ketentuan maka akan terus mengulang untuk meminta password baru sampai password yang sesuai dengan ketentuan berhasil didapat. setelah itu pada conditional if ```"$((match_index - ${#usernames[@]}))" == "0"``` disini match_index akan bernilai maximal adalah (jumlah user) - 1. Jika match_index bernilai sama dengan jumlah_user maka user tidak ditemukan didalam user.txt, itu artinya kita dapat menambahkan user baru kedalam user.txt lalu akan membuat log jika berhasil maupun error.

## Cara pengerjaan
Untuk logika dalam pembuatan soal ini cukup simple untuk membuatnya juga tidak perlu waktu lama. Pada awalnya saya membuat sangat modular dengan membagi menjadi beberapa service. Tepatnya 6 service yaitu service untuk base app, login, register, log writer, find username index, dan password checker. Saya membuat dengan gaya mini services. Tetapi saya baru sadar harus didalam 1 file. Akhirnya saya menjadikan file file tersebut menjadi 1 file, agar tetap terstruktur saya membuat beberapa function untuk membuat code lebih readable dan lebih rapih. kesulitan setelah itu adalah niat karna setelah saya dengan berat hati menggabungkan ke dalam 1 file saya harus memisahkan nya lagi menjadi 2 file karna pembaharuan ketentuan soal. Tetapi tidak ada masalah dan kendala apa apa dan memisahkannya hanya memakan waktu 10 menitan. 

## screenshot
### login.sh (gagal login)
![image](/uploads/23bce54fe0b1426ecc53f932fb336c55/image.png)
![image](/uploads/866c891703a3cb6eebc4f1c1446fccc3/image.png)

### login.sh (berhasil login)
![image](/uploads/866c891703a3cb6eebc4f1c1446fccc3/image.png)

### retep.sh (Membuat user baru dengan testcase password)
![image](/uploads/b4c7a7db6f736882c720e3b88eaf7cdb/image.png)
![image](/uploads/46f86559e61c3860931387a528d4d9bb/image.png)
