# Soal shift no. 1 : University_Survey
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan [file .csv](https://drive.google.com/file/d/1VkS-NVY4OwX4OsL4ydpL5uBBqJlJYiix/view?usp=sharing "csv file") yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
### a. Menampilkan 5 Universitas dengan ranking tertinggi di Jepang
```shell
grep "Japan" 2023unirank.csv | sort -t "," -k 1 -n | head -n 5 | 
awk -F "," '{print $2}'
```
**Program Output**
![output.1a](https://gitlab.com/laurivasyyy/sisop-praktikum-modul-1-2023-bs-d13/uploads/2d6d6f6fac9dbd1ebec9357b46dc73ca/1a.png)


`grep` <br />
digunakan untuk mencari baris yang mengandung kata "Japan" di file 2023unirank.csv <br />
`sort -t "," -k 1 -n` <br />
mengurutkan hasil filter berdasarkan field kolom 1 (Rank) dengan menggunakan koma sebagai separator (`-t`) dan 
mengurutkannya secara numerik (`-n`) <br />
`head -n 5` <br />
mengambil 5 baris pertama dari hasil `sort`<br />
`awk -F "," '{print $2}'`<br />
mencetak field kolom 2 (Nama Universitas) dari setiap baris data yang diperoleh sebelumnya dengan koma sebagai
separator (`-F`)

### b. Menampilkan 5 FSR Score yang paling rendah di universitas Jepang
```shell
grep "Japan" 2023unirank.csv | sort -t "," -k 9 -rn | tail -rn 5 |
awk -F "," '{print $2, $9}'
```
**Program Output**<br />
![output.1b](https://gitlab.com/laurivasyyy/sisop-praktikum-modul-1-2023-bs-d13/uploads/516a821290f2b418f3890c7135ec6215/1b.png)

`sort -t "," -k 9 -rn`<br />
melakukan `sort` untuk mengurutkan hasil filter berdasarkan field kolom 9 (FSR) dengan menggunakan koma sebagai 
separator (`-t`) lalu mengurutkannya secara numerik (`-n`) dan descending (`-r`) <br />
`tail -rn 5`<br />
mengambil 5 baris akhir dari hasil `sort`<br />
`awk -F "," '{print $2, $9}'`<br />
mencetak field kolom 2 (Nama Universitas) dan field kolom 9 (FSR) dari setiap baris data yang diperoleh sebelumnya 
dengan koma sebagai separator (`-F`)

### c. Mencari 10 Universitas di Jepang dengan GER paling tinggi
```shell
grep "Japan" 2023unirank.csv | sort -t "," -k 20 -n | head -n 10 | 
awk -F "," '{print $2, $20}'
```
**Program Output**
![output.1c](https://gitlab.com/laurivasyyy/sisop-praktikum-modul-1-2023-bs-d13/uploads/bf0e7aa3e2d5997943b0f9f71c51eb14/1c.png)

`grep` <br />
digunakan untuk mencari baris yang mengandung kata "Japan" di file 2023unirank.csv<br />
`sort -t "," -k 20 -n` <br />
mengurutkan hasil filter berdasarkan field kolom 20 (GER) dengan menggunakan koma sebagai separator (`-t`) dan 
mengurutkannya secara numerik (`-n`)<br />
`head -n 10`<br />
mengambil 10 baris pertama dari hasil `sort`<br />
`awk -F "," '{print $2, $20}'`<br />
mencetak field kolom 2 (Nama Universitas) dan field kolom 20 (GER rank) dari setiap baris data yang diperoleh 
sebelumnya dengan koma sebagai separator (`-F`)

### d. Mencari universitas dengan kata kunci 'keren'
``` shell
grep "Keren" 2023unirank.csv| awk -F "," '{print $2}'
```
**Program Output**
![output.1d](https://gitlab.com/laurivasyyy/sisop-praktikum-modul-1-2023-bs-d13/uploads/1b402adf81fac1d140f19d875586002d/1d.png)

`grep` <br />
digunakan untuk mencari baris yang mengandung kata "Japan" di file 2023unirank.csv<br />
`awk -F "," '{print $2}'`<br />
mencetak field kolom 2 (Nama Universitas) dari setiap baris data yang diperoleh dari `grep`
dengan koma sebagai separator (`-F`)
