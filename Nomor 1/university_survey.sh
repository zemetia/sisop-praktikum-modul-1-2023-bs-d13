#!/bin/bash
#1a Menampilkan 5 Universitas dengan ranking tertinggi di Jepang.
echo "Top 5 university in Japan :"
grep "Japan" 2023unirank.csv | sort -t "," -k 1 -n | head -n 5 | 
awk -F "," '{print $2}'

#1b Menampilkan FSR Score yang paling rendah diantara 5 universitas di Jepang.
echo -e "\n5 Lowest FSR Score university in Japan :"
grep "Japan" 2023unirank.csv | sort -t "," -k 9 -n | head -n 5 |
awk -F "," '{print $2, $9}'

#1c Mencari 10 Universitas di Jepang dengan GER paling tinggi.
echo -e "\nTop 10 University in Japan based on the highest ger rank :"
grep "Japan" 2023unirank.csv | sort -t "," -k 20 -n | head -n 10 | 
awk -F "," '{print $2, $20}'

#1d Mencari universitas dengan kata kunci 'keren'.
echo -e "\nUniversity contain 'keren' keywords :"
grep "Keren" 2023unirank.csv| awk -F "," '{print $2}'
