# Nomor 4
Pada bagian log_encrypt.sh
```bash
shift=0
ciphertext=""

```
Dua variabel pertama yang didefinisikan di sini, `shift` dan `ciphertext`, akan menyimpan jumlah pergeseran untuk enkripsi dan hasil enkripsi.

```bash
function shift_char {
    char=$1
    base=$2
    shift=$3

    # check if the character is a letter
    # Shift the letter by the specified number of positions
    ascii_code=$(printf '%d' "'$char")
    shifted_ascii_code=$(( (ascii_code - $base + $shift) % 26 + $base ))
    shifted_char=$(printf "\x$(printf %x $shifted_ascii_code)")
    # echo "$shifted_char"
    ciphertext="$ciphertext$shifted_char"
}

```
Fungsi `shift_char` melakukan enkripsi pada sebuah karakter. Fungsi ini akan mengubah `char` menjadi karakter terenkripsi dan menambahkannya ke variabel `ciphertext.`

```bash
function shifting {
    text=$1
    shift=$2

    plaintext=$text
    upper_base=$(printf '%d' "'A")
    lower_base=$(printf '%d' "'a")

    ciphertext=""
    for (( i=0; i<${#plaintext}; i++ )); do
        char="${plaintext:$i:1}"
        if [[ "$char" =~ [A-Z] ]]; then
            shift_char $char $upper_base $shift  
        elif [[ "$char" =~ [a-z] ]]; then
            shift_char $char $lower_base $shift
        else
            # Leave non-letter characters unchanged
            ciphertext="$ciphertext$char"
        fi
    done
}

```
Fungsi `shifting` melakukan enkripsi pada sebuah teks. Fungsi ini memanggil fungsi `shift_char` untuk setiap karakter di dalam `text`, dan menghasilkan teks terenkripsi dalam variabel `ciphertext`.

```bash
log=$(cat /var/log/syslog | tail -f)  # Syslog (just the tail of it, panjang banget)
shift=$(date +"%H")
shifting "$log" $shift

```
Fungsi `shifting` dipanggil dengan parameter `log` dan `shift` untuk melakukan enkripsi pada isi file syslog dan hasilnya disimpan dalam variabel `ciphertext`.

Pada bagian log_dencrypt.sh

```bash
backup_file=$(ls -t backup_date.txt | head -1)
```
Untuk bagian ini guna nya untuk membaca file backup_date.txt dan memilih file terbaru dari semua file backup yang ada.

```bash
ciphertext=$(cat $backup_file)
```
Fungsinya untuk membaca isi dari file backup yang dipilih sebelumnya dan menyimpannya pada variabel ciphertext.

```bash
backup_date=$(echo $backup_file | sed 's/\.txt//')
```
Pada bagian ini mengambil tanggal dari nama file backup yang telah dipilih dan menyimpannya pada variabel backup_date.

```bash
shift=$(date -d "$backup_date" +"%H")
```
Untuk mengambil jam dari tanggal file backup dan menyimpannya pada variabel shift.

```bash
plaintext=$(shifting "$ciphertext" $shift)
```
Dibagian ini mendekripsi pesan yang telah dienkripsi dengan menggunakan caesar cipher serta menyimpan pesan yang didekripsi pada variabel plaintext.


