shift=0
ciphertext=""

function shift_char {
    char=$1
    base=$2
    shift=$3

    # check if the character is a letter
    # Shift the letter by the specified number of positions
    ascii_code=$(printf '%d' "'$char")
    shifted_ascii_code=$(( (ascii_code - $base + $shift) % 26 + $base ))
    shifted_char=$(printf "\x$(printf %x $shifted_ascii_code)")
    # echo "$shifted_char"
    ciphertext="$ciphertext$shifted_char"
}

function shifting {
    text=$1
    shift=$2

    plaintext=$text
    upper_base=$(printf '%d' "'A")
    lower_base=$(printf '%d' "'a")

    ciphertext=""
    for (( i=0; i<${#plaintext}; i++ )); do
        char="${plaintext:$i:1}"
        if [[ "$char" =~ [A-Z] ]]; then
            shift_char $char $upper_base $shift  
        elif [[ "$char" =~ [a-z] ]]; then
            shift_char $char $lower_base $shift
        else
            # Leave non-letter characters unchanged
            ciphertext="$ciphertext$char"
        fi
    done
}

log=$(cat /var/log/syslog | tail -f)  # Syslog (just the tail of it, panjang banget)
shift=$(date +"%H")
shifting "$log" $shift

backup_date=$(date +"%H:%M %d:%m:%Y")
backup_name="$backup_date.txt"
echo $ciphertext >> "$backup_name"
