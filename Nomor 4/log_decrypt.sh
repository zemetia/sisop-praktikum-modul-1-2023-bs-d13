#!/bin/bash

# Fungsi untuk menggeser karakter
function shift_char {
    char=$1
    base=$2
    shift=$3

    # Periksa apakah karakter adalah huruf
    # Geser huruf sebanyak jumlah posisi yang ditentukan
    ascii_code=$(printf '%d' "'$char")
    shifted_ascii_code=$(( (ascii_code - $base - $shift + 26) % 26 + $base ))
    shifted_char=$(printf "\x$(printf %x $shifted_ascii_code)")
    echo -n "$shifted_char"
}

# Fungsi untuk mengembalikan pesan asli
function shifting {
    text=$1
    shift=$2

    plaintext=$text
    upper_base=$(printf '%d' "'A")
    lower_base=$(printf '%d' "'a")

    for (( i=0; i<${#plaintext}; i++ )); do
        char="${plaintext:$i:1}"
        if [[ "$char" =~ [A-Z] ]]; then
            shift_char $char $upper_base $shift  
        elif [[ "$char" =~ [a-z] ]]; then
            shift_char $char $lower_base $shift
        else
            # Biarkan karakter non-huruf tidak berubah
            echo -n "$char"
        fi
    done
}

# Baca log yang dienkripsi dari file backup
backup_file=$(ls -t backup_date.txt | head -1)
ciphertext=$(cat $backup_file)

# Dapatkan jumlah pergeseran yang digunakan pada saat enkripsi
backup_date=$(echo $backup_file | sed 's/\.txt//')
shift=$(date -d "$backup_date" +"%H")

# Dekripsi log yang telah dienkripsi
plaintext=$(shifting "$ciphertext" $shift)

# Tampilkan log yang telah didekripsi
echo "$plaintext"
